import numpy as np


# Activation functions

class Sigmoid:

    @staticmethod
    def calculate(z):
        return 1.0 / (1.0 + np.exp(-z))

    @staticmethod
    def calculate_prime(z):
        return Sigmoid.calculate(z) * (1 - Sigmoid.calculate(z))


class Softmax:

    @staticmethod
    def calculate(z):
        e_x = np.exp(z - np.max(z))
        return e_x / e_x.sum(axis=0)

    @staticmethod
    def calculate_prime(z):
        raise Exception


class ReLu:

    @staticmethod
    def calculate(z):
        return np.maximum(0, z)

    @staticmethod
    def calculate_prime(z):
        return np.where(z > 0, 1, 0)


class LeakyReLu:

    @staticmethod
    def calculate(z):
        return np.where(z > 0, z, z * 0.01)

    @staticmethod
    def calculate_prime(z):
        return np.where(z > 0, 1, 0.01)


class TanH:

    @staticmethod
    def calculate(z):
        return np.tanh(z)

    @staticmethod
    def calculate_prime(z):
        return TanH.calculate(z) - TanH.calculate(z) ** 2


class Linear:

    @staticmethod
    def calculate(z):
        return z

    @staticmethod
    def calculate_prime(z):
        return 0 * z + 1
