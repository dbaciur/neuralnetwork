python3 run.py -l 2,20,10,2 --task=class --epochs 30 --batch-size 10 --learning-rate 0.1 \
--training-data="data/classification/data.simple.train.100.csv" \
--test-data="data/classification/data.simple.test.100.csv"
