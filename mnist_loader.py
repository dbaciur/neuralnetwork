import struct
import pickle
import gzip
import numpy as np


def load_mnist():
    import numpy as np

    train_data = load_image_data('data/mnist/train-images.idx3-ubyte')
    train_labels = load_labels_data('data/mnist/train-labels.idx1-ubyte')

    test_data = load_image_data('data/mnist/t10k-images.idx3-ubyte')
    test_labels = load_labels_data('data/mnist/t10k-labels.idx1-ubyte')


    return (combine_data(train_data, train_labels, True), combine_data(test_data, test_labels, False))

def construct_output_vector(label):
    zero_vector = np.zeros((10, 1))

    zero_vector[label] = 1.0

    return zero_vector

def combine_data(data, labels, results_as_vector):
    return [(d, construct_output_vector(l) if results_as_vector else l)  for (d, l) in zip(data, labels)]

def load_image_data(path):
    with open(path, 'rb') as f:
        magic, size = struct.unpack(">II", f.read(8))
        nrows, ncols = struct.unpack(">II", f.read(8))
        data = np.fromfile(f, dtype=np.dtype(np.uint8).newbyteorder('>'))
        data = data.reshape((size, nrows * ncols))
        data = [np.reshape(x, (nrows * ncols, 1)) for x in data]
    
    return data

def load_labels_data(path):
    with open('data/mnist/train-labels.idx1-ubyte', 'rb') as f:
        magic, size = struct.unpack(">II", f.read(8))
        labels = np.fromfile(f, dtype=np.dtype(np.uint8).newbyteorder('>'))
    
    return labels
