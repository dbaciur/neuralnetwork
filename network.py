import numpy as np


class Network:

    def __init__(self, sizes, activation_f, cost_f, output_f, output_layer_activation_f, learning_rate, enable_biases):
        self.activation_f = activation_f
        self.cost_f = cost_f
        self.output_f = output_f
        self.learning_rate = learning_rate
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.weights = self.random_weights(sizes)
        self.output_layer_activation_f = output_layer_activation_f
        if enable_biases:
            self.biases = self.random_biases(sizes)
        else:
            self.biases = self.zero_biases(sizes)

    @staticmethod
    def random_weights(sizes):
        weights = []
        for layer_index in range(len(sizes) - 1):
            weights.append(np.random.randn(
                sizes[layer_index + 1], sizes[layer_index]))
        return weights

    @staticmethod
    def random_biases(sizes):
        return [np.random.randn(layer_size, 1) for layer_size in sizes[1:]]

    @staticmethod
    def zero_biases(sizes):
        return [np.zeros((layer_size, 1)) for layer_size in sizes[1:]]

    def predict(self, input_vector):
        network_output = self.get_network_output(input_vector)
        return self.output_f.calculate(network_output)

    def get_network_output(self, a):
        z_vectors, activation_vectors = self.calculate_layers_values(a)
        return activation_vectors[-1]

    def get_input_error(self, x, y):
        return self.cost_f.calculate(self.get_network_output(x), y)

    def train_on_batch(self, data_batch, epoch, gradient_output_file):
        batch_size = len(data_batch)
        batch_delta_b = [np.zeros(b.shape) for b in self.biases]
        batch_delta_w = [np.zeros(w.shape) for w in self.weights]

        for input_vector, result_vector in data_batch:
            delta_b, delta_w = self.back_propagation(input_vector, result_vector)
            batch_delta_b = [nb + dnb / batch_size for nb, dnb in zip(batch_delta_b, delta_b)]
            batch_delta_w = [nw + dnw / batch_size for nw, dnw in zip(batch_delta_w, delta_w)]


        self.weights = [layer_weight - self.learning_rate * layer_delta_w
                        for layer_weight, layer_delta_w in zip(self.weights, batch_delta_w)]

        self.append_weights_to_csv(gradient_output_file, epoch,
            [self.learning_rate * layer_delta_w for layer_delta_w in batch_delta_w])

        self.biases = [layer_bias - self.learning_rate * layer_delta_b
                       for layer_bias, layer_delta_b in zip(self.biases, batch_delta_b)]


    def calculate_layers_values(self, input_vector):
        activation_vectors = [input_vector]
        z_vectors = []

        activation_vector = input_vector

        layers_with_weights_count = len(self.weights)

        for layer_index in range(layers_with_weights_count):
            layer_weights = self.weights[layer_index]
            layer_biases = self.biases[layer_index]

            # z = W * x + b
            z = np.dot(layer_weights, activation_vector) + layer_biases
            z_vectors.append(z)

            # hidden layer
            if layer_index != layers_with_weights_count - 1:
                # e.g. sigmoid(z)
                activation_vector = self.activation_f.calculate(z)
            # output layer
            else:
                activation_vector = self.output_layer_activation_f.calculate(z)
            activation_vectors.append(activation_vector)

        return z_vectors, activation_vectors

    def back_propagation(self, input_vector, result_vector):
        delta_b = [np.zeros(b.shape) for b in self.biases]  # gradient for the cost function regarding biases
        delta_w = [np.zeros(w.shape) for w in self.weights]  # gradient for the cost function regarding weights

        (z_vectors, activation_vectors) = self.calculate_layers_values(input_vector)

        # C - cost function
        # R - activation function
        # a^L - activation for L layer
        # z^L = w^L * a^L + b^L

        # cost = C(a^L) = C(R(z^L)) = C(R(w^L * a^L-1))
        # cost' = C'(a^L) * R'(z^L) * (w^L * a^L-1)'

        # d_cost / d_b^L = C'(R) * R'(z^L)
        # d_cost / d_w^L = C'(R) * R'(z^L) * a^L
        # delta_propagation = C'(R) * R'(z^L)
        # delta_propagation = self.cost_f.calculate_prime(activation_vectors[-1], result_vector) \
        #                     * self.output_layer_activation_f.calculate_prime(z_vectors[-1])

        delta_propagation = self.cost_f.calculate_prime(
            activation_vectors[-1], result_vector,
            z_vectors[-1], self.output_layer_activation_f
        )

        delta_b[-1] = delta_propagation
        delta_w[-1] = np.dot(delta_propagation, activation_vectors[-2].transpose())

        # cost = C(R(w^L * a^L-1)) = C(R(w^L * R(z^L-1))) = C(R(w^L * R(w^L-1 * a^L-2)))
        # d_cost / d_b^L-1 = delta_propagation * w^L * R'(z^L-1)
        # d_cost / d_w^L-1 = delta_propagation * w^L * R'(z^L-1) * a^L-2
        for layer_index in range(2, self.num_layers):
            z = z_vectors[-layer_index]
            delta_propagation = np.dot(self.weights[-layer_index + 1].transpose(), delta_propagation) \
                                * self.activation_f.calculate_prime(z)
            delta_b[-layer_index] = delta_propagation
            delta_w[-layer_index] = np.dot(delta_propagation, activation_vectors[-layer_index - 1].transpose())

        return delta_b, delta_w

    def append_weights_to_csv(self, file, epoch, weights):
        file.write(f'Epoch {epoch + 1}\n')        

        for i in range(len(weights)):
            file.write(f'Layer: {i + 1}\n')

            for j in range(len(weights[i])):
                file.write(f'Neuron: {j}\n')
                neuron_weights = ",".join([str(weight) for weight in weights[i][j]])
                file.write(neuron_weights)
                file.write("\n")

            file.write("\n")
            