import numpy as np


class DataSet:
    def __init__(self, data_set):
        self.min = np.min(np.array([y for (x, y) in data_set]))
        self.max = np.max(np.array([y for (x, y) in data_set]))
        self.data_set = data_set
        self.normalized_data_set = self.normalize_set(data_set)

    def normalize_vector(self, v):
        return (v - np.min(v)) / (self.max - self.min) * 2 - 1

    def denormalize_vector(self, v):
        return (v + 1) * (self.max - self.min) / 2 + self.min

    def normalize_set(self, data_set):
        results = np.array([y for (x, y) in data_set])
        results = self.normalize_vector(results)

        normalized = []
        for data_row in range(len(data_set)):
            normalized.append((data_set[data_row][0], results[data_row]))
        return normalized

    def get_original_data(self):
        return self.data_set

    def get_normalized_data(self):
        return self.normalized_data_set
