import numpy as np
import matplotlib.cm as cm
from matplotlib.colors import ListedColormap, BoundaryNorm
import matplotlib.patches as mpatches
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from data_set import DataSet

class Visualization:

    @staticmethod
    def visualize_classification(network, test_data):
        mesh_step = 0.01
        # Create color maps
        cmap_light = ListedColormap(['#ffc9c9', '#AAFFAA', '#AAAAFF','#AFAFAF'])
        cmap_bold  = ListedColormap(['#FF0000', '#00FF00', '#0000FF','#7a7474'])

        x_range = np.arange(-1, 1, mesh_step)
        y_range = np.arange(-1, 1, mesh_step)

        xx, yy = np.meshgrid(x_range, y_range)

        z = list(map(
            lambda x: network.predict(np.array([x[0], x[1]]).reshape(2,1)),
            np.transpose([np.tile(x_range, len(y_range)), np.repeat(y_range, len(x_range))])))

        z = np.asarray(z).reshape(xx.shape)

        plt.figure()
        plt.pcolormesh(xx, yy, z, cmap=cmap_light)

        x = [x[0][0][0] for x in test_data]
        y = [x[0][1][0] for x in test_data]


        plt.scatter(x, y, s=5, c=list(map(lambda x: x[1], test_data)), cmap=cmap_bold)

        plt.show()

    @staticmethod
    def visualize_regression(network, test_data_set: DataSet, normalized_learning):
        test_data = test_data_set.get_original_data()

        mesh_step = 0.01
        plt.figure()

        x_range = np.arange(-5, 5, mesh_step)
        y_range = [network.predict(np.array([x]).reshape(1, 1)) for x in x_range]
        if normalized_learning:
            y_range = [test_data_set.denormalize_vector(y[0]) for y in y_range]

        x_test = [x[0][0][0] for x in test_data]
        y_test = [x[1] for x in test_data]

        plt.scatter(x_test, y_test, s=1)

        plt.scatter(x_range, y_range, s=1, c="#ff0000")

        plt.show()
