import argparse
import csv_loader
import network
import learning_session
import random
import numpy as np
import activation_functions
import cost_functions
import output_functions
import visualization as vs
import mnist_loader as ml
from data_set import DataSet


class Configuration(object):
    def __init__(self, arguments):
        self.layers = list(map(int, arguments.layers.split(',')))
        self.epochs = arguments.epochs
        self.learning_rate = arguments.learning_rate
        self.batch_size = arguments.batch_size
        self.seed = arguments.seed
        self.training_data = arguments.training_data
        self.test_data = arguments.test_data
        self.enable_biases = arguments.enable_biases == "t"
        self.task = arguments.task
        self.cost_f = self.get_cost_function_by_name(arguments.cost_function)
        self.activation_f = self.get_activation_function_by_name(arguments.activation_function)
        self.output_f = self.get_output_function_for_task(arguments.task)

    @staticmethod
    def get_activation_function_by_name(name):
        if name == "sigmoid":
            return activation_functions.Sigmoid
        if name == "relu":
            return activation_functions.ReLu
        if name == "leaky_relu":
            return activation_functions.LeakyReLu
        raise Exception

    @staticmethod
    def get_cost_function_by_name(name):
        if name == "quadratic":
            return cost_functions.QuadraticCost
        if name == "cross_entropy":
            return cost_functions.CrossEntropyCost
        raise Exception

    @staticmethod
    def get_output_function_for_task(task):
        if task == "class":
            return output_functions.DigitClassification
        if task == "reg":
            return output_functions.Regression
        raise Exception


def run_network(c: Configuration):
    mnist = ml.load_mnist()

    if c.task == "class":
        train_data_set = DataSet(csv_loader.load_training_data_for_classification(c.training_data))
    else:
        train_data_set = DataSet(csv_loader.load_training_data_for_regression(c.training_data))

    test_data_set = DataSet(csv_loader.load_data(c.test_data))

    train_data_set = DataSet(mnist[0])
    test_data_set = DataSet(mnist[1])

    random.seed(c.seed)
    np.random.seed(c.seed)

    normalized_learning = False

    net = network.Network(c.layers,
                          activation_f=c.activation_f,
                          cost_f=c.cost_f,
                          output_f=output_functions.DigitClassification if c.task == "class" else output_functions.Regression,
                          enable_biases=c.enable_biases,
                          learning_rate=c.learning_rate,
                          output_layer_activation_f=activation_functions.Sigmoid if c.task == "class" else activation_functions.Linear)
    learning = learning_session.LearningSession(network=net,
                                                is_classification=c.task == "class",
                                                training_data=train_data_set.get_normalized_data() if normalized_learning
                                                else train_data_set.get_original_data(),
                                                test_data=test_data_set.get_normalized_data() if normalized_learning
                                                else test_data_set.get_original_data(),
                                                epochs=c.epochs,
                                                batch_size=c.batch_size)
    learning.start_training()

    if (c.task == "class"):
        vs.Visualization.visualize_classification(net, test_data_set.get_original_data())
    else:
        vs.Visualization.visualize_regression(net, test_data_set, normalized_learning)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Multilayer perceptron")
    parser.add_argument("-l", "--layers", type=str,
                        help="array with size of each layer, e.g. [2,10,2]", required=True)

    parser.add_argument("--task", type=str,
                        help="classification or regression", required=True,
                        choices=["class", "reg"])

    parser.add_argument("--learning-rate", type=float,
                        help="seed", required=True)

    parser.add_argument("-e", "--epochs", type=int,
                        help="numer of epochs", required=True)

    parser.add_argument("--batch-size", type=int,
                        help="learning batch size", required=True)

    parser.add_argument("--training-data", type=str,
                        help="training data file path", required=True)

    parser.add_argument("--test-data", type=str,
                        help="test data file path", required=True)

    # not required
    parser.add_argument("-s", "--seed", type=int, default=0,
                        help="seed", required=False)

    parser.add_argument("--enable-biases", type=str, default="t",
                        help="enable / disable using biases",
                        choices=["t", "f"])

    parser.add_argument("--cost-function", type=str, default="quadratic",
                        help="cost function",
                        choices=["quadratic", "cross_entropy"])

    parser.add_argument("--activation-function", type=str, default="sigmoid",
                        help="activation function",
                        choices=["sigmoid", "relu", "leaky_relu"])

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    config = Configuration(args)
    run_network(config)
