import numpy as np


# Output functions

class DigitClassification:

    @staticmethod
    def softmax(z):
        e_x = np.exp(z - np.max(z))
        return e_x / e_x.sum(axis=0)

    @staticmethod
    def calculate(z):
        return np.argmax(DigitClassification.softmax(z)) + 1


class Regression:

    @staticmethod
    def calculate(z):
        return z
