import csv
import numpy as np


def load_training_data_for_regression(data_file_path):
    train_data = []
    for (input_vector, expected_result) in load_data(data_file_path):
        result_vector = np.array([expected_result])
        train_data.append((input_vector, result_vector))
    return train_data


def load_data(data_file_path):
    test_data = []
    with open(data_file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader, None)  # skip headers

        for row in csv_reader:
            row_length = len(row)

            input_variables_vector = np.array(
                as_floats_list(row[:row_length - 1])) \
                .reshape(row_length - 1, -1)

            expected_result = float(row[row_length - 1])

            test_data.append((input_variables_vector, expected_result))
    return test_data


def as_floats_list(strings_list):
    return list(map(float, strings_list))


def result_as_classification_vector(result_value, class_count):
    """ Returns 2-dimensional vector mask representing which of all possible results given result_value is,
        marking result_value index with 1.0 and all others with 0.
    1 -> [ 1.0,
           0.0 ]
    2 -> [ 0.0,
           1.0 ]
    """
    zero_vector = np.zeros((class_count, 1))

    result_value_index = int(result_value) - 1
    zero_vector[result_value_index] = 1.0

    return zero_vector


def get_class_count(data):
    results = np.array([y for (x, y) in data])
    return int(np.max(results) - np.min(results) + 1)


def load_training_data_for_classification(data_file_path):
    train_data = []
    data = load_data(data_file_path)
    class_count = get_class_count(data)
    for (input_vector, expected_result) in data:
        result_vector = result_as_classification_vector(expected_result, class_count)
        train_data.append((input_vector, result_vector))
    return train_data
