import numpy as np
import activation_functions


# Cost functions

class QuadraticCost:

    @staticmethod
    def calculate(a, y):
        return 0.5 * np.linalg.norm(a - y) ** 2

    @staticmethod
    def calculate_prime(a, y, z, activation_f):
        return (a - y) * activation_f.calculate_prime(z)


# class MeanAbsolute:
#
#     @staticmethod
#     def calculate(a, y):
#         return 0.5 * np.linalg.norm(a - y) ** 2
#
#     @staticmethod
#     def calculate_prime(a, y, z, activation_f):
#         return (a - y) * activation_f.calculate_prime(z)


class CrossEntropyCost(object):

    @staticmethod
    def calculate(a, y):
        return np.sum(
            np.nan_to_num(
                -y * np.log(a) - (1 - y) * np.log(1 - a)))

    @staticmethod
    def calculate_prime(a, y, z, activation_f):
        return a - y
