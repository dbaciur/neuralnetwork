import random
import numpy as np
import pylab


class LearningSession:

    def __init__(self, network, is_classification, training_data, epochs, batch_size, test_data):
        self.network = network
        self.is_classification = is_classification
        self.training_data = training_data
        self.epochs = epochs
        self.batch_size = batch_size
        self.test_data = test_data

        self.test_data_errors = []
        self.training_data_errors = []

    def start_training(self):
        print("Starting gradient descent training")
        self.log_epoch_stats(0)
        # self.save_data_set_errors()

        weights_output_file = open("weights.txt", "w")
        gradient_output_file = open("gradient.txt", "w")

        for i in range(self.epochs):
            training_batches = self.split_data_into_batches()
            for training_batch in training_batches:
                self.network.train_on_batch(training_batch, i, gradient_output_file)
                # self.save_data_set_errors()
            self.log_epoch_stats(i + 1)
            self.network.append_weights_to_csv(weights_output_file, i, self.network.weights)
        self.plot_data_sets_error()

        weights_output_file.close()
        gradient_output_file.close()

    def split_data_into_batches(self):
        np.random.shuffle(self.training_data)

        data_size = len(self.training_data)
        batch_start_index = 0

        batches = []
        while batch_start_index < data_size:
            batches.append(self.training_data[batch_start_index: batch_start_index + self.batch_size])
            batch_start_index += self.batch_size
        return batches

    def log_epoch_stats(self, epoch_number):
        if not self.is_classification:
            print("Epoch {} finished".format(epoch_number))
            return

        (recognition_vector, successful_recognitions) = self.check_network_classification_stats(self.test_data)
        print("Epoch {}: {} / {} \t {}".format(
            epoch_number, successful_recognitions, len(self.test_data), recognition_vector))

    def check_network_classification_stats(self, test_data):
        successful_recognitions = 0
        recognition_vector = [0] * self.network.sizes[-1]

        for (x, y) in test_data:
            recognized_digit = self.network.predict(x)

            if recognized_digit == y:
                successful_recognitions += 1
            recognition_vector[recognized_digit - 1] += 1

        return recognition_vector, successful_recognitions

    def save_data_set_errors(self):
        test_data_error = self.get_data_set_error(self.test_data)
        self.test_data_errors.append(test_data_error)

        training_data_error = self.get_data_set_error(self.training_data)
        self.training_data_errors.append(training_data_error)

    def get_data_set_error(self, data_set):
        data_set_probe = random.sample(data_set, int(1 * len(data_set)))
        probe_size = len(data_set_probe)
        data_set_error = [self.network.get_input_error(x, y) for (x, y) in data_set_probe]
        return np.average(data_set_error) / probe_size

    def plot_data_sets_error(self):
        pylab.plot(range(len(self.test_data_errors)), self.test_data_errors, 'b', label='Test data')
        pylab.plot(range(len(self.training_data_errors)), self.training_data_errors, 'g', label='Training data')

        pylab.legend(loc='best')
        pylab.xlabel('Number of weights [and biases] updates')
        pylab.ylabel('Data error')
        pylab.show()
